<?php
// if isset disini berfungsi untuk memeriksa apakah variabel submit sudah tersedia atau belum jika sudah akan melanjutkan ke pendefisian penginputan jika belum akan muncul alert
if (isset($_POST['submit']))
{
    
// variabel yang di $_POST berfungsi untuk memanggil data yang telah di inputkan agar bisa di panggil.
$nama   = $_POST['nama'];
$matapelajaran  = $_POST['matapelajaran'];
$nilaiuts  = $_POST['nilaiuts'];
$nilaiuas  = $_POST['nilaiuas'];
$nilaitugas    = $_POST['nilaitugas'];

// berfungsi untuk melakukan penghitungan nilai berdasarkan presentase setiap nilai dengan nilai yang diinputkan
$uts = $nilaiuts * 0.35;
$uas = $nilaiuas * 0.50;
$tugas = $nilaitugas * 0.15;

// berfungsi untuk menjumlahkan seluruh nilai yang telah dihitung berdasarkan presentase menjadi nilai akhir/ nilai total
$nilai_total = $uts + $uas + $tugas;

// berfungsi untuk menentukan grade setiap rentang nilai
if ($nilai_total>=90){$grade = 'A';}
elseif ($nilai_total>=70){$grade = 'B';}
elseif ($nilai_total>=50){$grade = 'C';}
else{$grade = 'D';}

}
else
{
    // berfungsi untuk menampilkan alert atau peringatan karena belum mengisi data serta meredirect ke file input.php
    echo "<script>alert('Tidak dapat mengakses halaman ini, anda harus mengisi data pada form ini terlebih dahulu. Klik Ok untuk menuju ke form!');
    window.location.href='input.php';</script>";
}

 
?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>1915091020_Sang Putu Yoga Pramana</title>
</head>

<body>
    <br>
    <!-- div dengan class container berfungsi untuk membungkus kode lain didalamnya agar terlihat lebih rapi dan sesuai dengan ukuran -->
    <div class="container border border-primary">
        <br>
         <!-- h2 digunakan sebagai judul output -->
        <h2>Hasil Pengolahan Nilai Mahasiswa</h2>
        <!-- tabel berfungsi untuk merapikan output dalam bentuk grid yang terdiri atas kolom dan baris  -->
        <table class="table">
            <!-- berfungsi untuk memangil variabel $nama yang sesuai dengan yang diinputkan di halaman sebelumnya -->
            <tr>
                <td class="col-sm-2">Nama</td>
                <td class="col-sm-4">: <?php echo "$nama"; ?></td>
            </tr>
            <!-- berfungsi untuk memangil variabel $matapelajaran yang sesuai dengan yang diinputkan di halaman sebelumnya -->
            <tr>
                <td class="col-sm-2">Mata Pelajaran</td>
                <td class="col-sm-4">: <?php echo "$matapelajaran"; ?></td>
            </tr>
            <!-- berfungsi untuk memangil variabel $niaiuts yang sesuai dengan yang diinputkan di halaman sebelumnya -->
            <tr>
                <td class="col-sm-2">Nilai UTS</td>
                <td class="col-sm-4">: <?php echo "$nilaiuts"; ?></td>
            </tr>
            <!-- berfungsi untuk memangil variabel $nilaiuas yang sesuai dengan yang diinputkan di halaman sebelumnya -->
            <tr>
                <td class="col-sm-2">Nilai UAS</td>
                <td class="col-sm-4">: <?php echo "$nilaiuas"; ?></td>
            </tr>
            <!-- berfungsi untuk memangil variabel $nilaitugas yang sesuai dengan yang diinputkan di halaman sebelumnya -->
            <tr>
                <td class="col-sm-2">Nilai Tugas</td>
                <td class="col-sm-4">: <?php echo "$nilaitugas"; ?></td>
            </tr>
            <!-- berfungsi untuk memangil variabel $nilai_total yang sesuai perhitungan seluruh nilai -->
            <tr>
                <td class="col-sm-2">Total Nilai</td>
                <td class="col-sm-4">: <?php echo "$nilai_total"; ?></td>
            </tr>
            <!-- berfungsi untuk memangil variabel $grade yang sesuai dengan rentang nilai yang sudah ditentukan -->
            <tr>
                <td class="col-sm-2">Grade</td>
                <td class="col-sm-4">: <?php echo "$grade"; ?></td>
            </tr>
        </table>
        <div class="mb-3">
            <!-- berfungsi sebagai tombol kembali ke halaman sebelumnya -->
            <button class="btn btn-primary" type="button">
                <a class="text-decoration-none text-light" href="input.php">Kembali</a>
            </button>
        </div>

        <br>
    </div>
</body>

</html>
