<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>1915091020_Sang Putu Yoga Pramana</title>
</head>

<body>
    <br>
    <!-- div dengan class container berfungsi untuk membungkus kode lain didalamnya agar terlihat lebih rapi dan sesuai dengan ukuran -->
    <div class="container border border-primary">
        <!-- h2 digunakan sebagai judul form -->
        <h2>Pengolahan Nilai Mahasiswa</h2>
        <!-- form action "output.php" menjelaskan bahwa nantinya form yang sudah kita inputkan datanya akan dikirimkan ke file output.php -->
        <!-- form method "post" maksudnya untuk mengirimkan nilai/data ke file lain, dan post juga berfungsi agar data yang diinputkan tidak dapat diliat melalui link karena bersifat rahasia/penting -->
        <form action="output.php" method="post">
            <!-- div ini berfungsi untuk membungkus sebuah inputan agar telihat lebih rapi dilayar -->
            <div class="mb-3">
                <!-- label berguna untuk memberikan identitas mengenai inputan dibawahnya -->
                <label for="nama" class="form-label">Nama</label>
                <!-- input disini berfungsi untuk menampung inputan nama yang bertipe text dengan name="nama" yang nantinya akan di panggil di outputnya. lalu required berfungsi agar form yang diinputkan tidak boleh kosong-->
                <input type="text" name="nama" class="form-control" id="nama" required>
            </div>
            <div class="mb-3">
                <!-- label berguna untuk memberikan identitas mengenai inputan dibawahnya -->
                <label for="matapelajaran" class="form-label">Mata Pelajaran</label>
                <!-- input disini berfungsi untuk menampung inputan nama yang bertipe text dengan name="matapelajaran" yang nantinya akan di panggil di outputnya. lalu required berfungsi agar form yang diinputkan tidak boleh kosong -->
                <input type="text" name="matapelajaran" class="form-control" id="matapelajaran" required>
            </div>
            <div class="mb-3">
                <!-- label berguna untuk memberikan identitas mengenai inputan dibawahnya -->
                <label for="nilaiuts" class="form-label">Nilai UTS</label>
                <!-- input disini berfungsi untuk menampung inputan nama yang bertipe number dengan name="nilaiuts" yang nantinya akan di panggil di outputnya. lalu required berfungsi agar form yang diinputkan tidak boleh kosong. serta terdapat pembatasan inputan dengan minimal nilai 0 dan maksimal 100 -->
                <input type="number" name="nilaiuts" class="form-control" id="nilaiuts" min="0" max="100" required>
            </div>
            <div class="mb-3">
                <!-- label berguna untuk memberikan identitas mengenai inputan dibawahnya -->
                <label for="nilaiuas" class="form-label">Nilai UAS</label>
                <!-- input disini berfungsi untuk menampung inputan nama yang bertipe number dengan name="nilaiuas" yang nantinya akan di panggil di outputnya. lalu required berfungsi agar form yang diinputkan tidak boleh kosong. serta terdapat pembatasan inputan dengan minimal nilai 0 dan maksimal 100 -->
                <input type="number" name="nilaiuas" class="form-control" id="nilaiuas" min="0" max="100" required>
            </div>
            <div class="mb-3">
                <!-- label berguna untuk memberikan identitas mengenai inputan dibawahnya -->
                <label for="nilaitugas" class="form-label">Nilai Tugas</label>
                <!-- input disini berfungsi untuk menampung inputan nama yang bertipe number dengan name="nilaitugas" yang nantinya akan di panggil di outputnya. lalu required berfungsi agar form yang diinputkan tidak boleh kosong. serta terdapat pembatasan inputan dengan minimal nilai 0 dan maksimal 100 -->
                <input type="number" name="nilaitugas" class="form-control" id="nilaitugas" min="0" max="100" required>
            </div>
            <div class="mb-3">
                <!-- button bertipe submit berfungsi untuk memproses data yang diinputkan -->
                <button type="submit" name="submit" class="btn btn-success">Submit</button>
                <!-- button bertipe reset berfungsi untuk mengosongkan inputan apabila terjadi kesalahan input -->
                <button type="reset" class="btn btn-danger">Reset</button>
            </div>
        </form>
        <br>
    </div>
</body>

</html>
